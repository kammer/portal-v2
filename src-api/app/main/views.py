import uuid

from flask import Flask, jsonify, request, flash

from app.models import User, ClassRoom
from app import db

from . import main

@main.route('/registraSala', methods=['GET', 'POST'])
def registraSala():
    response_object = {'status': 'success'}
    if request.method == 'POST':
        post_data = request.get_json()
        new_class = ClassRoom(
            name= post_data.get('className'),
            number_of_seats= post_data.get('nbSeats'),
        )
        db.session.add(new_class)
        db.session.commit()

        response_object['message'] = 'Sala Registrada'
    else:
        classes = ClassRoom.query.all()
        _list = []
        for classRoom in classes:
            class_data = {
                'id': classRoom.id,
                'name': classRoom.name,
                'nbSeats': classRoom.number_of_seats
            }
            _list.append(class_data)
        response_object['salas'] = _list
    return jsonify(response_object)