import uuid

from flask_jwt_extended import create_access_token
from flask import Flask, jsonify, request, flash

from app.models import User, Role
from app import db

from . import auth

@auth.route('/usuario', methods=['GET', 'POST'])
def registraUsuario():
    response_object = {'status': 'success'}
    if request.method == 'POST':
        post_data = request.get_json()
        user = User(
            name= post_data.get('name'),
            username= post_data.get('username'),
            email= post_data.get('email'),
            password= post_data.get('password')
        )
        db.session.add(user)
        db.session.commit()

        response_object['message'] = 'Usuário Registrado'
    else:
        users = User.query.all()
        _list = []
        for user in users:
            user_data = {
                'id': user.id,
                'name': user.name,
                'email': user.email,
                'username': user.username
            }
            _list.append(user_data)
        response_object['usuario'] = _list
    return jsonify(response_object)

@auth.route('/usuario/login', methods=['POST'])
def login():
    post_data = request.get_json()
    username = post_data.get('username')
    password = post_data.get('password')

    userData = User.query.filter_by(username=username).first()
    if userData is not None and userData.verify_password(password):
        role_name = Role.query.filter_by(id=userData.role_id).first()
        access_token = create_access_token(
            identity={
                'name': userData.name,
                'username': userData.username,
                'email': userData.email,
                'permission': role_name.name
            })
        result = access_token
    else:
        result = jsonify({"error": "Usuario ou senha inválidos!"})

    return result