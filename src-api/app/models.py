from datetime import datetime

from werkzeug.security import check_password_hash, generate_password_hash

from app import db

class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)

    def __repr__(self):
        return '<Role %r>' % self.name

class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), unique=True, index=True)
    name = db.Column(db.String(64))
    email = db.Column(db.String(128), unique=True, index=True)
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))
    password_hash = db.Column(db.String(128))

    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)
        if self.role_id is None:
            roleData = Role.query.filter_by(name='Aluno').first()
            self.role_id = roleData.id

    def __repr__(self):
        return '<User %r>' % self.username

    @property
    def password(self):
        raise AttributeError('password is not readable')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

class ClassRoom(db.Model):
    __tablename__ = 'classroom'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128))
    number_of_seats = db.Column(db.String(128))

# class StudentSeat(db.Model):
#     __tablename__ = 'studentseat'
#     id = db.Column(db.Integer, primary_key=True)
#     seat_id = db.Column(db.Integer, db.ForeignKey('user.id'))
#     student_id = db.Column(db.Integer, db.ForeignKey('user.id'))

# class StudentSeat(db.Model):
#     __tablename__ = 'studentseat'
#     id = db.Column(db.Integer, primary_key=True)
#     seat_id = db.Column(db.Integer, db.ForeignKey('user.id'))
#     student_id = db.Column(db.Integer, db.ForeignKey('user.id'))

