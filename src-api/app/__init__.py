from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from config import Config
from flask_migrate import Migrate
from flask_jwt_extended import JWTManager


db = SQLAlchemy()
config = Config()

def create_app():
    app = Flask(__name__)
    jwt = JWTManager(app)
    CORS(app, resources={r'/*': {'origins': '*'}})
    app.config.from_object(config)
    config.init_app(app)
    migrate = Migrate(app, db)

    db.init_app(app)

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    return app
