# Portal

> Projeto para a Matéria de Desenvolvimento Web

## Iniciar o servidor

### Vá para src-nuxt
``` bash
# Instalar dependencias
$ npm install

# Iniciar servidor no modo DEV no endereço: localhost:3000 
$ npm run dev
```

### Vá para src-api
``` bash
# Criar uma venv
$ python3 -m venv (nome da venv) 

# Ativar a venv
$ . (nome da venv)/bin/activate

# Instalar dependencias
$ pip3 install -r requirements.txt

# Exportar variaveis de ambiente
$ export FLASK APP=app.py

# Iniciar servidor no modo DEV no endereço: localhost:5000
$ flask run
```

## Frameworks, Linguagens e afins utilizados no projeto

>* Python 3,
>* Flask,
>* VueJS,
>* Nuxt,
>* JavaScript,
>* NodeJs

## Proposta

> Reserva de Salas do campus

    Criar um portal onde professores possam reservar salas do campus, assim como alunos podem reservar uma cadeira em uma sala de um determinado professor / podem solicitar
    uma reserva de sala aos administradores.
    
## Grupos:

### Todos:
>* Editar Perfil
>* Ver salas disponíveis
>* Ver cadeiras disponíveis

### Alunos:
>* Poderão reservar um lugar na sala
>* Ver qual aula terá na sala
>* Fazer uma solicitação de reserva (a ser confirmada pelo adm)

### Professores:
>* Poderão fazer reserva imediata da sala, se ela estiver livre
>* Poderão definir o limite de pessoas da sala (cada sala também tem um limite máximo)

### ADM (Ti):
>* Cadastro de salas
>* Aprovação de reservas para alunos
>* Atualização de status de sala
>* Atualizações cadastrais

> Todo novo usuário se cadastra como aluno, e é “promovido” pelo adm para professor


